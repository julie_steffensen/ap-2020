function setup() {
  // put setup code here
  createCanvas(640,480);
  print("hello world");
  background('rgba(0,255,0, 0.25)');

}
function draw() {
  // put drawing code here
  fill('rgb(0,255,0)');
  noStroke();
  rect(100, 20, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(-40, 20, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(30, 20, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(170, 20, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(240, 20, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(310, 20, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(380, 20, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(450, 20, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(520, 20, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(590, 20, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(-40, 100, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(30, 100, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(100, 100, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(170, 100, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(240, 100, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(310, 100, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(380, 100, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(450, 100, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(520, 100, 55, 55, 20);

  fill('rgb(0,255,0)');
  noStroke();
  rect(590, 100, 55, 55, 20);

  fill('rgba(100%,0%,100%,0.5)');
  circle(30, 30, 20);

  fill('rgba(100%,0%,100%,0.5)');
  circle(100, 30, 20);

  fill('rgba(100%,0%,100%,0.5)');
  circle(170, 30, 20);

  fill('rgba(100%,0%,100%,0.5)');
  circle(240, 30, 20);

  fill('rgba(100%,0%,100%,0.5)');
  circle(310, 30, 20);

  fill('rgba(100%,0%,100%,0.5)');
  circle(380, 30, 20);

  fill('rgba(100%,0%,100%,0.5)');
  circle(450, 30, 20);

  fill('rgba(100%,0%,100%,0.5)');
  circle(520, 30, 20);

  fill('rgba(100%,0%,100%,0.5)');
  circle(590, 30, 20);

  fill(255, 204, 0);
  ellipse(200, 200, 300, 300);

  fill('#fae');
  ellipse(185,200,150,150);

  noFill();
  colorMode(RGB, 255, 255, 255, 1);

  strokeWeight(4);
  stroke(255, 0, 10, 0.3);
  ellipse(250,250,100,100);
  ellipse(250,250,70,70);


}
