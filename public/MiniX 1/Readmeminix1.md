![screenshot](MiniX_1_billede.png)

https://julie_steffensen.gitlab.io/ap-2020/MiniX%201/

I started out tying to draw shapes and make place them in relation to each other. I wanted to draw a somewhat controlled sketch using different shapes and colors. My main fokus in this assignment was the different color and shape and I consciously did not add any movement. Instead I focused on having control and fully understanding the code I wrote. I got inspiration from references but stayed away from examples. I did this to have a better understanding of each individual code. After this assignment I feel confident writing this code freehand without reference.
