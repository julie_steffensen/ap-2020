![screenshot](billede.png)

https://julie_steffensen.gitlab.io/ap-2020/MiniX%206

<h2>This week sucked!</h2>

This week I really wanted to understand and execute the code from the beginning, and it was challenging but after spending a lot of hours I made the two images appear. Then I mode on to.  Multiplying the bunny and nothing worked! I first made a “if” statement. 

if (framecount<1) {
 this.push = new Bunny
}

This did not clear the screen but it did not worked because I could not get the Bunny position to be random.  

let BunnySize = {
W:0,
h:0
};

let Bunny;
let Eng;
let BunPosY = 450;
let BunPosX = 430;

function draw() {
image(Bunny,BunPosX,BunPosY,BunnySize.w,BunnySize.h);
}

If I put random in front of any of the BunPosX or BunPosY then my bunny would disappear so if my “if” statement worked, I would not know because it would just create new bunny’s in the same place. 

So I tried making a class instead where I put 
image(Bunny,BunPosX,BunPosY,BunnySize.w,BunnySize.h);
down in show() and then created a class exactly like winnies and like other I found online but it did not work. Then I tried to make an ellipse instead of a bunny for show() because maybe the. problem was the image, but it still did not work.  

Then I tried to copy all Winnies code to start over and then slowly change her code instead, so I copied both codes and put them in two different .js files. I changed the Index.html so but .js files where included and uploaded the packman.gif to my folder and when I ran the sketch it was just a clear screen. 

By this time, I was so tired and frustrated and I had no idea why it was not working and the peers I saw did not know the problem either. I needed real help from Ann or Noah. 

So, I decided to leave it because I was getting nowhere and make a really long readme instead. 

What is a game? 

We started discussing this in the instructor class on Wednesday. “what constitutes a game?” my initial thought was some kind of hero or protagonist on a mission with a goal and an enemy. All these categories do not have to be human or even human like. Although the protagonist should usually be humanized in some way so the player can empathize with it. The enemy should does not have to humanized because the played does not have to emphasize with this object so it can be everything. 

After discussing this in groups Freyja made me realize that it was a very narrow-minded perception of games that I have. Her favorite game was some kind of Tetris-Soduko game that did not fit my perception of a game. Or maybe it did? The protagonist in this case is the person played, the tools is the shapes, the goal is to eliminate any gaps and the enemy is the gaps. This understanding is more abstract then my first perception and definition but maybe it still works.  

<h2>My Game</h2>

My game should have been a bunny multiplying to the speed of the framecount. For every framecount a new bunny should appear. Then I would upload a picture of an axe and let it follow the mouse position inspired by Sophia MiniX 4. Then every time you would click a bunny it. Would disappear or “splice” as it would be as a syntax. You would lose every time the number of bunnies on the screen would be reach too many. There would be a counter for every bunny you have killed and therefor is there no end goal but the reason for playing would be setting a record. 

<h2>Why this game?</h2>

 I thought it was funny to create an enemy that was by perception cute and usually thought of as something positive. Instead I focused on the overpopulation that occurs in nature and bunnies are a prime example of this. The game would be slaughtering bunnies which is a bit macabre, but this can also be the thing that makes it funny. Everything about the game is so cute except the goal. 
