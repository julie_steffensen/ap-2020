![screenshor](MiniX_3_billede.png)


https://julie_steffensen.gitlab.io/ap-2020/MiniX%203

This time I did not start out with a plan. Instead I took inspiration from Winnis eksample from class 03 and Daniels video on the bouncing ball. I combined the two syntaxes and tweaked it until I found the throbber pleasing and. Interesting to look at. 

First, I defined a variable x that make the ellipses move away from the circle and the speed which defines how fast it goes. The num variable defines how many ellipses appear. I sat the framerate low as in Winnis Eksemple and gave the background a low alpha value to create the faded effect. 

I played around with making my own function although It does not have any purpose in this miniX I wanted to try it out so I can apply it in the next miniX. 

The time related syntaxes in my program is “360/num*(frameCount%num)” this defines how far the ellipses appear from each other. The framerate itself affect the speed of the ellipses the faster the framerate the faster my program will run. 


I am pretty satisfied with my trobber I can look at it for a long time, but I wished I would have worked more with arrays and I hope to work that in with the next miniX 
