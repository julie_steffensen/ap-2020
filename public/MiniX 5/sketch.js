function preload() {

img =loadImage('GRIS1.png');

}

let bubbles = [];
//[] viser at det er inden for et interval som tæller fra 0


function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(8)

}

function mousePressed() {
let r =random(10,50);
let b = new Bubble(mouseX,mouseY,r);
bubbles.push(b);
//grunden til at jeg bruger push er at det tilføjer til arrayen så den ikke hele tiden sletter og starter forfra
}

function draw() {
  background(255);
  image(img,80,40);
  for(let i = 0; i < bubbles.length; i++) {
  bubbles[i].move();
  bubbles[i].show();
  }
}

class Bubble {
  //find ud af definitionen på en constructor
  constructor(x,y,r) {
  this.x = x;
  this.y = y;
  this.r = r;
}
move() {
  this.x = this.x + random(0,20);
  this.y = this.y + random(0,20);
}

show() {
  stroke(255);
  fill(random(0,196,255,200));
  ellipse(this.x,this.y,this.r*2);
}
}
