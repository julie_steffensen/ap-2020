![screenshot](MiniX5_billede.png)

https://julie_steffensen.gitlab.io/ap-2020/MiniX%205

<h3> Describing my code </h3>

For this week’s miniX assigment i wanted to better understand functions and syntaxes in p5 js. I feel that I don’t understand code all and every week I struggle to make the design and ideas in my head.  So for this week I wants to understand objects and arrays and how they can work together with images. 

First of all I wanted to plays with arrays so that is why I made the “bubbles=[]” with is an array which contains the all index values starting from 0. Then I defined the functions” move” and “show”. First, I construct my variables x, y, and r for the movement and size of my bubbles. For move I define the coordinates x and y to equal itself and add a random value between 0 and 20.  By not choosing a negative value the Bubbles can only move forward and therefor appears to be disappearing because they are leaving frame. 

For the the show function I created an ellipse with random color fill and alpha value to create some fun and exciting to look at. 

For the image I edited a cartoon of a pig in photoshop saved it in my p5js. folder and uploaded it to my sketch. The reason I wanted to explore this is because in all other miniX assignment I have tried to create images and forcingly been trying to draw In p5.js but inspired be Sophia’s miniX from last week I discovered that for some things it is better to upload a picture edited or created in a more suitable program to get the wanted effect. 

<h3>Why I made this sketch </h3>
This week I wanted to create a background of a cartoon pig and when the image was clicked on smaller images of other cartoon pigs would appear on the screen. I did not know how to change tot bubbles tot images, but I almost reached my goal. The reason for my design Is that I have lost faith in my coding skills and I no longer see the fun in coding as I did in the beginning, so I decided to redo the first miniX and do something that I think is fun. Even though I did not fully succeed with my design I really enjoyed making this miniX and I got some of my excitement back.

<h3>What I have learned this week</h3>

This week we read the forthcoming of the book” Preface in Aesthetic Programming: A handbook of Software Studies” by winnie Soon. Which made me think again why I need to learn how to code. It was nice to reflect on this once more because I know so much more now than I did in week 1. That in itself is a motivator but for me learning to code has changed med perspective on all my software. I know know how things are created and understand that everything In my computer is some kind of created illusion that is designed for me to respond a certain way. This understanding I think is key for me if I one day want to become a good designer of digital artifacts. 

Through software studies i have also been introduced to code and programming language as language that in the future might will be used a natural alnguage for comunication between humans. Also relflecting on the assignment to create a button last week i now understand the  appeal of using open source softwares so i dont thave to make predecided binary choises which are being tracked. To use these open source technologies i have to know code. 


