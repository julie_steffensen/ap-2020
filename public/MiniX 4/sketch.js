let button1;
let input, button2, greet;

function setup() {

  createCanvas(windowWidth,windowHeight);
  background(208,226,236);

  button1 = createButton("Watch!");
  button1.mouseClicked(moveButton);
  button1.size(90,30);
  button1.position(20,400);
  button1.style("font-family", "Bodoni");
  button1.style("font-size", "20px");

  input = createInput();
  input.position(20, 90);

  textSize(10);
  text('Full Name', 20, 85);

  input = createInput();
  input.position(20, 130);

  textSize(10);
  text('Phone Number', 20, 125);

  input = createInput();
  input.position(20, 170);
  textSize(10);
  text('Email', 20, 165);

  input = createInput();
  input.position(20, 210);
  textSize(10);
  text('Favorite pet', 20, 205);

  input = createInput();
  input.position(20, 250);
  textSize(10);
  text('Shoe size', 20, 245);

  input = createInput();
  input.position(20, 290);
  textSize(10);
  text('Favorite food', 20, 285);

  input = createInput();
  input.position(20, 330);
  textSize(10);
  text('Tings you like to do', 20, 325);

  input = createInput();
  input.position(20, 370);
  textSize(10);
  text('Something funny', 20, 365);

  greeting = createElement('h2', 'You have been tagged in a video fill out information to watch');
  greeting.position(20, -10);

  textAlign(CENTER);
  textSize(50);

  }
function moveButton() {
  button1.position (random(width), random(height));
}
