![screenshot](MIniX_2_billede.png)

---

https://julie_steffensen.gitlab.io/ap-2020/MiniX%202

---

When I use emojis I rarely use the faces available, instead i use the flowers, stars and animals to represent a good mood. Without having to consider which exact smiley mood I am in.
I usually find my mood in between the options available. I am not laughing or blushing, but I am in a good mood. Likewise, I feel like the poop represent a bad mood or situation and the little chicken represent helpless and confused. 
 
That is why I chose to work of my own experience and make emojis which represents emotions in a more metaphorical way. I chose to make a flower and a smiley with at flower mouth. 
Both emojis is in pink tones which universally represents friendship, affection, harmony. This again gives in to the intension to signal good vibes on a more metaphorical level. 

I wanted to make two smileys that does not resemble any human being. But something more abstract. But try incorporate some universally good associations, like flowers and light colors. 

I used P5 playground to make the smiley and everything in it. I discovered that this was a great tool for me when I want to draw something and make a more controlled design.
I tried to add a varible in the form of the rotation function but it would have liked to have used that some more. 

For the next MiniX i want to make my design move, and use varibles more controlled. 

--- 

Julie Steffensen 
