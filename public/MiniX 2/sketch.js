function setup() {
createCanvas(1500,1500);
background('rgb(238, 232, 255)');
}
function draw() {

  stroke(255);
  translate(300, 300);
  for (let i =0; i < 25; i ++) {
  fill('rgb(243, 211, 232)');
  beginShape();

  ellipse(0,125,46,100);
  endShape();
  rotate(PI/10)
  fill('rgb(194, 226, 215)');
  ellipse(0,10,100,0,5)

  fill('rgb(255, 249, 102)');
  ellipse(5,10,30,0,15)

  fill('rgb(249, 178, 174)');
  ellipse(60,5,20,20,10)

}
rotate(4.70);
translate(120,-300);
fill('rgb(255, 232, 159)');
ellipse(326,313,265,255);
//hovedet
fill('rgb(255, 255, 255)');
ellipse(338,271,30,54);
ellipse(307,279,30,40);
//øjnene
fill(0)
stroke('rgb(99, 212, 212)');
ellipse(336,283,11,30);
ellipse(308,287,11,29);
//iriserne
stroke('#fae');
strokeWeight(4);
fill('rgb(235, 191, 212)');
ellipse(305,373,39,7);
ellipse(354,370,40,7);
ellipse(329,392,9,34);
ellipse(329,352,7,33);
//bladene
bezier(361,348,308,362,332,388,361,348);
bezier(289,348,324,387,349,364,289,348);
bezier(368,397,327,351,313,384,368,397);
bezier(294,399,352,373,319,360,294,399);
//blonstermund

stroke(255);
fill(255);
ellipse(342,276,7,10);

fill('rgb(255, 127, 159)');
triangle(219,228,330,178,224,124);
//hatten

fill('rgb(255, 127, 159)');
stroke('rgb(255, 127, 159)');
ellipse(329,371,12,9);
//midten af blomsten

ellipse(382,317,3,2);
ellipse(388,302,2,1);
ellipse(370,306,2,1);
//fregner

translate(-120,10);
ellipse(382,317,3,2);
ellipse(388,302,2,1);
ellipse(370,306,2,1);


if (mouseIsPressed)
console.log(mouseX,mouseY);

}
