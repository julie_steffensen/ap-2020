let r, g, b;
let x=0;
let speed=3;

function setup() {
  createCanvas(windowWidth, windowHeight);
  // Pick colors randomly
  r = random(255);
  g = random(255);
  b = random(255);
}

function draw() {
  background(230,230,250);
  strokeWeight(2);
  stroke(r, g, b);
  fill(r, g, b, 127);
  textSize(32);
  text('CATCH ME', x+50, 135);
  rect(x, 100, 260, 50);

  if(x<0 || x>200){
  speed=speed*-1;
  }
  x=x+1;
}
// When the user clicks the mouse
function mousePressed() {
  // Check if mouse is inside the circle
  let d = dist(mouseX, mouseY, 360, 200);
  if (d < 100) {
    // Pick new random color values
    r = random(255);
    g = random(255);
    b = random(255);
  }
}
