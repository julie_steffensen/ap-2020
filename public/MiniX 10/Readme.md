
<h1>Flowchart</h1>

<p>I have made 3 flowcharts. The first one i have made individually exploring the technical side of my code for MiniX4 (the thropper). The other two we made in my study group based on our two main ideas for our final project. The first flowchart is a storyttelling game and the other is a game that allows the user to modify the avatar.</p>  

<p>All three flowcharts can be found the the MiniX10 folder.</p> 

<h3>Flowchart for my thropper</h3>
<p>I made the flowchart for my thropper since I believe it it some of my more complicated code where I combined two different moving functions. In addition to that the code was not that long which I found very beneficial since I had trouble making a shot and simple flowchart even though the code was only about 30 lines. I also found it challenging to figuring out what is important to know when reading a flowchart. I do think It works well as a way to give an overall explanation on how the program is made.</p> 

<h3>Flowchart for Final Project</h3> 
<p>We started out discussing our final project last week where we talked about making a game and we did not get any further but had the assignment to think about what kind of game we want to create. When we start talking about the first idea the flowchart worked as a great tool to communicate our ideas among the members in our group. Usually when we discuss ideas our concepts tend to be fluffier and we sometimes misunderstand each other. This is where the flowchart forced us to discuss our program in detail.</p> 
 

<p>I have not though that much about how we are going to execute the program. We have looked on  some reference examples but I wanted us to imagine the our project have we would like it to be and then we can make cuts latter in the process if we find out some elements might be too hard to code.</p> 
