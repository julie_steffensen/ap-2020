var balls = [];
var theta;
var speed;
var stars = [];

function setup() {
createCanvas(windowWidth, windowHeight);
  colorMode(HSB);
  noStroke();
  fill(random(360), 100,100, 0.25);
  background(30)

  // for loop is used to create multiple ball objects
	for (var i = 0; i < 50; i++) {
    balls[i] = new Ball(width/2, height/2);
  }
  for (var i = 0; i < 1000; i++) {
  		stars[i] = new Star();
  	}
}

function draw() {

var galaxy = {
locationX : random(width),
locationY : random(height),
size : random(20,10)
}
for (var i = 0; i < stars.length; i++) {
  stars[i].draw();
}
  for (var i = 0; i < balls.length; i++) {
    stroke(frameCount %200,250, 100);
    ellipse(galaxy.locationX ,galaxy.locationY, galaxy.size, galaxy.size);
    balls[i].update();
    balls[i].display();
    balls[i].bounce();
  }

push();
translate(width * 0.5, height * 0.5);
rotate(frameCount / 200.0);
star(0, 0, 5, 100, 5);
pop();
}

function Ball(x, y) {
	this.x = x;
	this.y = y;
	this.sz = 20;
	this.xspeed = random(-10, 10);
	this.yspeed = random(-5, 5);

	this.update = function() {
		this.x += this.xspeed;
		this.y += this.yspeed;
	};

	this.display = function() {
    quad(windowWidth/2,this.sz, this.sz);
    line(this.x+1000, this.y, this.sz, this.sz);
    star(this.x,this.y,1, 1);
	};

	this.bounce = function() {
		if (this.x > width || this.x < 0) {
			this.xspeed *= -1;
		}
		if (this.y > height || this.y < 0) {
			this.yspeed *= -1;
		}
	}
}

function star(x, y, radius1, radius2, npoints) {
  let angle = TWO_PI / npoints;
  let halfAngle = angle / 8.0;
  beginShape();
  for (let a = 0; a < TWO_PI; a += angle) {
    let sx = x + cos(a) * radius2;
    let sy = y + sin(a) * radius2;
    vertex(sx, sy);
    sx = x + cos(a + halfAngle) * radius1;
    sy = y + sin(a + halfAngle) * radius1;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}
class Star {
	constructor() {
		this.x = random(width);
		this.y = random(height);
		this.size = random(0.25, 3);
		this.t = random(TAU);
	}

	draw() {
		this.t += 0.1;
		var scale = this.size + sin(this.t) * 2;
		noStroke();
		rect(this.x, this.y, scale, scale);
	}
}
